/****************************************************************

 data.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "data.h"

static void test_data_add(void)
{
    deeplearn learner;
    int no_of_inputs=10;
    int no_of_hiddens=4;
    int hidden_layers=2;
    int no_of_outputs=2;
    float error_threshold[] = { 0.01f, 0.01f, 0.01f };
    unsigned int random_seed = 123;
    char ** inputs_text = 0;

    printf("test_data_add...");

    /* create the learner */
    deeplearn_init(&learner,
                   no_of_inputs, no_of_hiddens,
                   hidden_layers,
                   no_of_outputs,
                   error_threshold,
                   &random_seed);

    assert((&learner)->net!=0);
    assert((&learner)->autocoder!=0);

    /* add some data samples */
    for (int i = 0; i < 5; i++)
    {
        float * inputs = (float*)malloc(no_of_inputs*sizeof(float));
        float * outputs = (float*)malloc(no_of_outputs*sizeof(float));
        for (int j = 0; j < no_of_inputs; j++)
        {
            inputs[j] = i;
        }
        for (int j = 0; j < no_of_outputs; j++)
        {
            outputs[j] = j;
        }
        assert(deeplearndata_add(&learner.data,
                                 &learner.data_samples,
                                 inputs, inputs_text,
                                 outputs, no_of_inputs, no_of_outputs,
                                 learner.input_range_min,
                                 learner.input_range_max,
                                 learner.output_range_min,
                                 learner.output_range_max) == 0);
        free(inputs);
        free(outputs);
    }
    assert(learner.data_samples == 5);
    assert(learner.indexed_data_samples == 0);

    assert(deeplearndata_index_data(
               learner.data,
               learner.data_samples,
               &learner.indexed_data,
               &learner.indexed_data_samples) == 0);
    assert(learner.indexed_data_samples == 5);

    /* check that the samples have the expected values.
       Note that the sequence is reversed */
    for (int i = 0; i < 5; i++)
    {
        deeplearndata * sample = deeplearndata_get(&learner, 4-i);
        assert(sample != 0);
        if ((int)sample->inputs[0] != i)
        {
            printf("(int)sample->inputs[0] %d != %d\n",
                   (int)sample->inputs[0], i);
        }
        assert((int)sample->inputs[0] == i);
        for (int j = 0; j < no_of_outputs; j++)
        {
            assert((int)sample->outputs[j] == j);
        }
    }

    /* free memory */
    deeplearn_free(&learner);

    printf("Ok\n");
}

static void test_data_training_test(void)
{
    deeplearn learner;
    int no_of_inputs=10;
    int no_of_hiddens=4;
    int hidden_layers=2;
    int no_of_outputs=2;
    float error_threshold[] = { 0.01f, 0.01f, 0.01f };
    unsigned int random_seed = 123;
    char ** inputs_text = 0;

    printf("test_data_training_test...");

    /* create the learner */
    deeplearn_init(&learner,
                   no_of_inputs, no_of_hiddens,
                   hidden_layers,
                   no_of_outputs,
                   error_threshold,
                   &random_seed);

    assert((&learner)->net!=0);
    assert((&learner)->autocoder!=0);

    /* add some data samples */
    for (int i = 0; i < 100; i++)
    {
        float * inputs = (float*)malloc(no_of_inputs*sizeof(float));
        float * outputs = (float*)malloc(no_of_outputs*sizeof(float));
        for (int j = 0; j < no_of_inputs; j++)
        {
            inputs[j] = i;
        }
        if ((i != 54) && (i != 79) && (i != 23))
        {
            /* labeled sample */
            for (int j = 0; j < no_of_outputs; j++)
            {
                outputs[j] = j;
            }
        }
        else
        {
            /* unlabeled sample */
            for (int j = 0; j < no_of_outputs; j++)
            {
                outputs[j] = DEEPLEARN_UNKNOWN_VALUE;
            }
        }
        assert(deeplearndata_add(&learner.data,
                                 &learner.data_samples,
                                 inputs, inputs_text, outputs,
                                 no_of_inputs, no_of_outputs,
                                 learner.input_range_min,
                                 learner.input_range_max,
                                 learner.output_range_min,
                                 learner.output_range_max) == 0);
        free(inputs);
        free(outputs);
    }
    assert(learner.data_samples == 100);
    assert(learner.indexed_data_samples == 0);

    assert(deeplearndata_index_data(
               learner.data,
               learner.data_samples,
               &learner.indexed_data,
               &learner.indexed_data_samples) == 0);
    assert(learner.indexed_data_samples == 100);

    assert(learner.training_data_samples == 0);
    assert(learner.training_data_labeled_samples == 0);
    assert(learner.test_data_samples == 0);
    assert(learner.indexed_training_data_samples == 0);
    assert(learner.indexed_training_data_labeled_samples == 0);
    assert(learner.indexed_test_data_samples == 0);
    assert(deeplearndata_create_datasets(&learner, 20) == 0);
    assert(learner.training_data_samples == 80);
    assert(learner.training_data_labeled_samples == 77);
    assert(learner.test_data_samples == 20);
    assert(learner.indexed_training_data_samples == 80);
    assert(learner.indexed_training_data_labeled_samples == 77);
    assert(learner.indexed_test_data_samples == 20);

    /* check that all test samples are labeled */
    for (int i = 0; i < learner.test_data_samples; i++)
    {
        deeplearndata * test_sample_labeled =
            deeplearndata_get_test(&learner, i);
        assert(test_sample_labeled->labeled == 1);
    }

    /* check that all labeled training samples are labeled */
    for (int i = 0; i < learner.training_data_labeled_samples; i++)
    {
        deeplearndata * training_sample_labeled2 =
            deeplearndata_get_training_labeled(&learner, i);
        assert(training_sample_labeled2->labeled == 1);
    }

    deeplearndata * training_sample =
        deeplearndata_get_training(&learner, 24);
    assert(training_sample != 0);
    for (int j = 0; j < no_of_outputs; j++)
    {
        assert(training_sample->outputs[j] == j);
    }
    deeplearndata * training_sample_labeled =
        deeplearndata_get_training_labeled(&learner, 22);
    assert(training_sample_labeled != 0);
    for (int j = 0; j < no_of_outputs; j++)
    {
        assert(training_sample_labeled->outputs[j] == j);
    }
    deeplearndata * test_sample =
        deeplearndata_get_test(&learner, 11);
    assert(test_sample != 0);
    for (int j = 0; j < no_of_outputs; j++)
    {
        assert(test_sample->outputs[j] == j);
    }

    /* free memory */
    deeplearn_free(&learner);

    printf("Ok\n");
}

int run_tests_data(void)
{
    printf("\nRunning data tests\n");

    test_data_add();
    test_data_training_test();

    printf("All data tests completed\n");
    return 0;
}
