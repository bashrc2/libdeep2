/****************************************************************

 deepconvnet.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "tdeepconvnet.h"

static void test_deepconvnet_init(void)
{
    /* convolution network */
    int no_of_convolutions = 3;
    int image_width = 64;
    int image_height = 64;
    int image_depth = 1;
    int max_features = 4*4;
    int feature_width = 4;
    int final_image_width = 4;
    int final_image_height = 4;
    float error_threshold[] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
    unsigned int random_seed = 123;
    unsigned int layer_itterations = 1000;

    /* deep learner */
    int no_of_deep_layers = 3;
    int no_of_outputs = 2;

    deepconvnet convnet;

    printf("test_deepconvnet_init...");

    assert(deepconvnet_init(no_of_convolutions,
                            no_of_deep_layers,
                            image_width,
                            image_height,
                            image_depth,
                            max_features,
                            feature_width,
                            final_image_width,
                            final_image_height,
                            layer_itterations,
                            no_of_outputs,
                            &convnet,
                            error_threshold,
                            &random_seed) == 0);


    deepconvnet_free(&convnet);

    printf("Ok\n");
}

int run_tests_deepconvnet(void)
{
    printf("\nRunning deepconvnet tests\n");

    test_deepconvnet_init();

    printf("All deepconvnet tests completed\n");
    return 0;
}
