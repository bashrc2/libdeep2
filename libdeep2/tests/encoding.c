/****************************************************************

 encoding.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "encoding.h"

int enc_text_to_binary(char * text,
                       bp_neuron ** inputs, int no_of_inputs,
                       int offset,
                       size_t max_field_length_chars);

static void test_encode_text(void)
{
    printf("test_encode_text...");

    char * text1 = "one";
    char * text2 = "two";
    char * text1_binary = "111101100111011010100110--------";
    char * text2_binary = "001011101110111011110110--------";
    bp_neuron ** inputs;
    int no_of_inputs = CHAR_BITS*8;
    int i, offset = 0, new_offset;
    int max_field_length_chars = 4;

    inputs = (bp_neuron**)malloc(no_of_inputs*sizeof(bp_neuron*));
    assert(inputs);
    for (i = 0; i < no_of_inputs; i++)
    {
        inputs[i] = (bp_neuron*)malloc(sizeof(bp_neuron));
        assert(inputs[i]);
    }

    new_offset = enc_text_to_binary(text1,
                                    inputs, no_of_inputs,
                                    offset,
                                    max_field_length_chars);
    assert(new_offset == 4*CHAR_BITS);
    offset = new_offset;

    new_offset = enc_text_to_binary(text1,
                                    inputs, no_of_inputs,
                                    offset,
                                    max_field_length_chars);
    assert(new_offset == 8*CHAR_BITS);

    /* check that the two encodings were the same */
    for (i = 0; i < 3*(int)CHAR_BITS; i++)
    {
        assert(inputs[i]->value == inputs[i+(4*(int)CHAR_BITS)]->value);
    }

    /* check the expected binary */
    for (i = 0; i < 4*(int)CHAR_BITS; i++)
    {
        if (inputs[i]->value < 0.4f)
        {
            assert(text1_binary[i] == '0');
        }
        if (inputs[i]->value > 0.6f)
        {
            assert(text1_binary[i] == '1');
        }
        if ((inputs[i]->value > 0.4f) &&
                (inputs[i]->value < 0.6f))
        {
            assert(text1_binary[i] == '-');
        }
    }

    offset = 0;
    new_offset = enc_text_to_binary(text2,
                                    inputs, no_of_inputs,
                                    offset,
                                    max_field_length_chars);
    assert(new_offset == 4*CHAR_BITS);

    /* check the expected binary */
    for (i = 0; i < 3*(int)CHAR_BITS; i++)
    {
        if (inputs[i]->value < 0.4f)
        {
            assert(text2_binary[i] == '0');
        }
        if (inputs[i]->value > 0.6f)
        {
            assert(text2_binary[i] == '1');
        }
        if ((inputs[i]->value > 0.4f) &&
                (inputs[i]->value < 0.6f))
        {
            assert(text2_binary[i] == '-');
        }
    }

    /*
    printf("\n");
    for (i = 0; i < 3*(int)CHAR_BITS; i++) {
        if (inputs[i]->value < 0.5f) {
            printf("0");
        }
        else {
            printf("1");
        }
    }
    printf("\n");
    */

    /* free memory */
    for (i = 0; i < no_of_inputs; i++)
    {
        free(inputs[i]);
    }
    free(inputs);

    printf("Ok\n");
}

int run_tests_encoding(void)
{
    printf("\nRunning encoding tests\n");

    test_encode_text();

    printf("All encoding tests completed\n");
    return 0;
}
