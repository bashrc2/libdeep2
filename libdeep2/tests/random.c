/****************************************************************

 random.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "random.h"

static void test_rand_num(void)
{
    unsigned int random_seed=0;
    unsigned int v,min=0,max=0;
    unsigned int test1[50];
    unsigned int test2[50];
    unsigned int test3[50];
    unsigned int * result = 0L;
    int same=0,repeats=0;

    printf("test_rand_num...");

    /* run three sequences with different seeds */
    for (int t = 0; t < 3; t++)
    {
        switch(t)
        {
        case 0:
        {
            random_seed = 123;
            result = (unsigned int*)test1;
            break;
        }
        case 1:
        {
            random_seed = 555;
            result = (unsigned int*)test2;
            break;
        }
        case 2:
        {
            random_seed = 8323;
            result = (unsigned int*)test3;
            break;
        }
        }
        for (int i = 0; i < 50; i++)
        {

            if (result)
            {
                result[i] = rand_num(&random_seed);
            }
        }
    }

    for (int i = 0; i < 50; i++)
    {
        /* check that the sequences are different */
        if ((test1[i]==test2[i]) ||
                (test1[i]==test3[i]) ||
                (test2[i]==test3[i]))
        {
            same++;
        }

        /* check the number of repeats within each sequence */
        for (int j = 0; j < 50; j++)
        {
            if (i!=j)
            {
                if ((test1[i]==test1[j]) ||
                        (test2[i]==test2[j]) ||
                        (test3[i]==test3[j]))
                {
                    repeats++;
                }
            }
        }
    }
    assert(same < 2);
    assert(repeats < 2);

    /* check that the range is not too restricted */
    for (int i = 0; i < 10000; i++)
    {
        v = rand_num(&random_seed);
        if ((i==0) ||
                ((i>0) && (v<min)))
        {
            min = v;
        }
        if ((i==0) ||
                ((i>0) && (v>max)))
        {
            max = v;
        }
    }
    assert(max > min);
    assert(min >= 0);
    assert(max - min > 60000);

    printf("Ok\n");
}

int run_tests_random(void)
{
    printf("\nRunning random number generator tests\n");

    test_rand_num();

    printf("All random number generator tests completed\n");
    return 0;
}
