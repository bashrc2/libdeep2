/****************************************************************

 autocoder.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/
#include "tautocoder.h"

static void test_autocoder_init(void)
{
    ac autocoder;
    int no_of_inputs = 32;
    int no_of_hiddens = 24;
    unsigned int random_seed = 5322;

    printf("test_autocoder_init...");

    assert(autocoder_init(&autocoder,
                          no_of_inputs,
                          no_of_hiddens,
                          random_seed) == 0);

    for (int i = 0; i < no_of_inputs*no_of_hiddens; i++)
    {
        assert(autocoder.weights[i] > -0.3f);
        assert(autocoder.weights[i] < 0.3f);
    }
    for (int h = 0; h < no_of_hiddens; h++)
    {
        assert(autocoder.bias[h] > -0.3f);
        assert(autocoder.bias[h] < 0.3f);
    }

    autocoder_free(&autocoder);

    printf("Ok\n");
}

static void test_autocoder_save_load(void)
{
    ac autocoder, autocoder_loaded;
    int no_of_inputs = 32;
    int no_of_hiddens = 24;
    unsigned int random_seed = 5322;
    unsigned int random_seed_loaded = 7269;
    FILE * fp;

    printf("test_autocoder_save_load...");

    assert(autocoder_init(&autocoder,
                          no_of_inputs,
                          no_of_hiddens,
                          random_seed) == 0);

    fp = fopen("/tmp/autocoder_test.dat","w");
    assert(fp);
    assert(autocoder_save(fp, &autocoder)==0);
    fclose(fp);

    assert(autocoder_init(&autocoder_loaded,
                          no_of_inputs,
                          no_of_hiddens,
                          random_seed_loaded) == 0);

    fp = fopen("/tmp/autocoder_test.dat","r");
    assert(fp);
    assert(autocoder_load(fp, &autocoder_loaded, 0)==0);
    fclose(fp);

    for (int i = 0; i < no_of_inputs*no_of_hiddens; i++)
    {
        assert(autocoder.weights[i] ==
               autocoder_loaded.weights[i]);
    }
    for (int i = 0; i < no_of_hiddens; i++)
    {
        assert(autocoder.bias[i] ==
               autocoder_loaded.bias[i]);
    }

    autocoder_free(&autocoder);
    autocoder_free(&autocoder_loaded);

    printf("Ok\n");
}

static void test_autocoder_update(void)
{
    ac autocoder;
    int no_of_inputs = 32;
    int no_of_hiddens = 24;
    unsigned int random_seed = 5322;

    printf("test_autocoder_update...");

    assert(autocoder_init(&autocoder,
                          no_of_inputs,
                          no_of_hiddens,
                          random_seed) == 0);
    autocoder.dropout_percent = 0.1f;
    autocoder.noise = 0;

    /* set some inputs */
    for (int i = 0; i < no_of_inputs; i++)
    {
        autocoder_set_input(&autocoder, i, NEURON_LOW + ((i/(float)no_of_inputs)*NEURON_RANGE));
    }

    /* some initial updates, because sometimes error initially increases */
    for (int t = 0; t < 100; t++)
    {
        autocoder_update(&autocoder);
    }
    float initialErrorPercent = autocoder.backprop_error_percent;
    assert(initialErrorPercent > 0);
    assert(initialErrorPercent < 100);
    for (int t = 0; t < 100; t++)
    {
        autocoder_update(&autocoder);
    }
    float secondErrorPercent = autocoder.backprop_error_percent;
    assert(secondErrorPercent > 0);
    assert(secondErrorPercent < 100);
    if (secondErrorPercent >= initialErrorPercent)
    {
        printf("\n1st %f  2nd %f\n",
               initialErrorPercent, secondErrorPercent);
    }
    assert(secondErrorPercent < initialErrorPercent);

    for (int t = 0; t < 100; t++)
    {
        autocoder_update(&autocoder);
    }
    float thirdErrorPercent = autocoder.backprop_error_percent;
    assert(thirdErrorPercent > 0);
    assert(thirdErrorPercent < 100);
    assert(thirdErrorPercent < secondErrorPercent);

    autocoder_free(&autocoder);

    printf("Ok\n");
}

int run_tests_autocoder(void)
{
    printf("\nRunning autocoder tests\n");

    test_autocoder_init();
    test_autocoder_save_load();
    test_autocoder_update();

    printf("All autocoder tests completed\n");
    return 0;
}
