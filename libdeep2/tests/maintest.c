/****************************************************************

 maintest.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tautocoder.h"
#include "tdeepconvnet.h"

#include "random.h"
#include "backprop.h"
#include "deeplearn.h"
#include "data.h"
#include "images.h"
#include "encoding.h"
#include "features.h"
#include "conv.h"
#include "deepconvnet.h"
#include "deeptransform.h"
#include "autocoder.h"

#define NEEDS_NO_FILES

int main(int argc, char* argv[])
{

    system("rm training.png");

    run_tests_autocoder();
    run_tests_backprop();
    run_tests_images();
    run_tests_random();
    run_tests_deeplearn();
    run_tests_data();
    run_tests_encoding();
    run_tests_features();
    run_tests_conv();
    run_tests_deepconvnet();
    run_tests_deeptransform();

    printf("\nAll tests completed\n");

    return 0;
}
