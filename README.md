# libdeep2

*libdeep2* is a machine learning analysis package for making [London1940.org](https://london1940.org/) buildings look like agent friendly buildings.

*libdeep* is a great C/C++ library for deep learning created by my friend and occasional coding colleague Bob Mottram. I need to make an optimized ML method for making buildings in my [London1940.org](https://london1940.org/) project look like buildings.

Here’s my remit.

*libdeep2* makes *libdeep* - makes the examples and unit tests that libdeep is famous for amongst those wanting to learn. libdeep2 also identifies [Bob Mottram](https://gitlab.com/bashrc2) specific code and my additional optimizations.

libdeep2 may take from my [ApeSDK](https://gitlab.com/barbalet/apesdk/) and also will probably interface with london1940.org map data. A large part of this map data from JSON is thanks to [Bob Mottram](https://gitlab.com/bashrc2). So basically Bob’s efforts and generosity are throughout this project and this is in no way to deny his genius and generous friendship. 

Tom Barbalet, July 30, 2022.

# libdeep: C/C++ library for deep learning

This is a C library which can be used in deep learning applications.  What differentiates libdeep from the numerous other deep learning systems out there is that it's small and that trained networks can be exported as *completely standalone C or Python programs* which can either be used via the commandline or within an Arduino IDE for creating robotics or IoT applications.

Installation
============

On Debian based systems:

```bash
sudo apt-get install build-essential doxygen xdot
```

If you want to be able to visualize call graphs for development or debugging purposes then you will need to install the [egypt](http://www.gson.org/egypt) script.

On Arch based systems:

``` bash
sudo pacman -S gcc doxygen egypt xdot
```

To build from source:

```bash
make
sudo make install
```

This creates the library and installs it into /usr/local

Unit Tests
==========

You can run the unit tests to check that the system is working as expected:

```bash
cd unittests
make
./tests
```

Or to check for any memory leaks:

```bash
valgrind --leak-check=full ./tests
```

Source Documentation
====================

To generate source code documentation make sure that you have doxygen installed and then run the generatedocs.sh script.  A subdirectory called docs will be created within which html and latex formated documentation can be found.  For general usage information you can also see the manpage.

```bash
man libdeep
```

Text or Numeric
===============

Inputs within training data sets can include both text and/or numeric data. For example, within the XOR training data you can have something like:

``` text
0.0,0.0,0.0
0.0,1.0,1.0
1.0,0.0,1.0
1.0,1.0,0.0
```

or alternatively, using whatever text representations you prefer:

``` text
zero,zero,0.0
zero,one,1.0
one,zero,1.0
one,one,0.0
```

Showing the call graph
======================

If you want to visualize the call graph for a particular source file for debugging or development purposes:

``` bash
SOURCEFILE=deeplearn.c make graph
```

And you can change the *SOURCEFILE* value to whatever file you're interested in.

Examples
========

There are also some example programs within the examples directory. Reading the examples is the best way to learn how to use this library within your own code. Examples are:

 * Face recognition with a deep convolutional network
 * Automatic feature learning and image reconstruction from features
 * Determining whether a cancer is malignant or benign
 * Assessing wine quality from ingredients
 * Predicting concrete quality from ingredients

Using trained neural nets in your system
========================================

You can export trained neural nets either as a C program or a Python program. These programs are completely independent and can be used either as commands or integrated into a larger software application. This makes it easy to use the resulting neural net without needing to link to libdeep. See the source code in the examples directory for how to use the export function. If you include the word "sketch" or "arduino" within the filename to be exported to then it will appear as Arduino compatible C suitable for use within an Arduino IDE rather than as standard C.

In your training program:

``` C
deeplearn_export(&learner, "export.c");
```

Then when training is done:

``` bash
gcc -o export_c export.c -lm
./export_c [first input] [second input]...
```

Portability
===========

Although this software was primarily written to run on Linux-based systems it's pretty much just vanilla C99 standard code and so it should be easily portable to other platforms, such as Microsoft Windows and Mac systems. The independent random number generator should mean that results are consistent across different compilers and platforms.

The following is the gitlab CI/CD scripts:

unit_test:
  stage: test
  script:
    - cd unittests
    - make
    - ./tests

cancerdetect_test:
  stage: test
  script:
    - cd examples/cancerdetect/
    - make all
    
cellnuclei_test:
  stage: test
  script:
    - cd examples/cellnuclei/
    - make all
    - ./cellnuclei

concreteslump_test:
  stage: test
  script:
    - cd examples/concreteslump/
    - make all    
    
facerec_test:
  stage: test
  script:
    - cd examples/facerec/
    - make all
    - ./facerec

features_test:
  stage: test
  script:
    - cd examples/features/
    - make all
    - ./features

iris_test:
  stage: test
  script:
    - cd examples/iris/
    - make all
    - ./iris

winequality_test:
  stage: test
  script:
    - cd examples/winequality/
    - make all
 
xor_test:
  stage: test
  script:
    - cd examples/xor/
    - ./test.sh   

These all run on Mac command line too.

This project is updated for current users as much as possible. Thanks to [Bob Mottram](https://gitlab.com/bashrc2) for providing the momentum to maintain this amazing source code.
