
./astyle --style=allman libdeep/*.c
./astyle --style=allman libdeep/*.h

./astyle --style=allman libdeep2/*.c
./astyle --style=allman libdeep2/*.h

./astyle --style=allman libdeep2/tests/*.c
./astyle --style=allman libdeep2/tests/*.h

./astyle --style=allman examples/xor/*.h
./astyle --style=allman examples/xor/*.c

./astyle --style=allman examples/winequality/*.c
./astyle --style=allman examples/iris/*.c
./astyle --style=allman examples/features/*.c
./astyle --style=allman examples/facerec/*.c
./astyle --style=allman examples/concreteslump/*.c
./astyle --style=allman examples/cellnuclei/*.c
./astyle --style=allman examples/cancerdetect/*.c

./astyle --style=allman examples/winequality/*.h
./astyle --style=allman examples/iris/*.h
./astyle --style=allman examples/features/*.h
./astyle --style=allman examples/facerec/*.h
./astyle --style=allman examples/concreteslump/*.h
./astyle --style=allman examples/cellnuclei/*.h
./astyle --style=allman examples/cancerdetect/*.h